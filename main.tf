## Variables
# aws
variable "profile" {}
variable "region" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}

# app
variable "app_path" {}
variable "dockerfile_path" {}

# ecr
variable "repository_list" {}
variable "docker_img_version" {}
# variable "aws_ecr_url" {}


## Init
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

provider "aws" {
  profile    = var.profile
  region     = var.region
#  access_key = var.aws_access_key
#  secret_key = var.aws_secret_key
}

# Get Account ID, User ID, and ARN
data "aws_caller_identity" "current" {}

# Get ecr repo token
data "aws_ecr_authorization_token" "token" {}

# aws_ecr_url = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com"

provider "docker" {
  registry_auth {
    address  = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com"
    username = data.aws_ecr_authorization_token.token.user_name
    password = data.aws_ecr_authorization_token.token.password
  }
}



## Create ECR repository
resource "aws_ecr_repository" "repository" {
  for_each = toset(var.repository_list)
  name     = each.key
}

## Build docker images and push to ECR
resource "docker_registry_image" "my_static_portfolio" {
  for_each = toset(var.repository_list)
  name     = "${aws_ecr_repository.repository[each.key].repository_url}:${var.docker_img_version}"

  build {
    context    = var.app_path
    dockerfile = var.dockerfile_path
    # dockerfile = "${each.key}.Dockerfile"
  }
}

## Setup proper credentials to push to ECR
