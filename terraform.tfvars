# aws
profile        = "evandjefie"
region         = "us-west-2"
#aws_access_key = ""
#aws_secret_key = ""

# app
app_path        = "/home/vanse/docker_project/my-static-portfolio/app"
dockerfile_path = "Dockerfile"

# ecr
repository_list    = ["evandjefie/my-static-portfolio"]
docker_img_version = "1.0.1"
# aws_ecr_url = ""

